FROM python:3.8.0-alpine

RUN apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd

RUN apk add postgresql

WORKDIR /usr/src/root

COPY ./requirements.txt /usr/src/root/requirements.txt
RUN pip install -r requirements.txt

COPY ./entrypoint.sh /usr/src/root/entrypoint.sh
RUN chmod +x /usr/src/root/entrypoint.sh

COPY . /usr/src/root

CMD /usr/src/root/entrypoint.sh
