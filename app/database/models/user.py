from .base import Base
from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    String,
    Text
)


class User(Base):
    __tablename__ = 'users'

    username = Column(String(100), nullable=False)
    email_address = Column(String(255), nullable=False)
    description = Column(Text(), nullable=True)
