
from config import get_config

from sqlalchemy import create_engine
from sqlalchemy.orm import (
    Session as SessionType,
    scoped_session,
    sessionmaker
)

config = get_config()
engine = create_engine(config.DATABASE_URI, pool_pre_ping=True)
Session = scoped_session(sessionmaker(bind=engine, expire_on_commit=False))
