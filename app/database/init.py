import sys
from sqlalchemy import create_engine

from models import Base    


if __name__ == '__main__':
    _, database_uri = sys.argv
    engine = create_engine(database_uri, echo=True)
    Base.metadata.create_all(engine)
