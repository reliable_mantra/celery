import os
from flask import Flask

from logger import setup_logger
from config import get_config_class_name
from database.session import Session
from tasks import init_queue
from blueprints.core import core


setup_logger()


def create_app(**kwargs):
    app = Flask(__name__)

    if kwargs.get('queue'):
        init_queue(kwargs.get('queue'), app)

    app.register_blueprint(core)

    config_class_name = get_config_class_name(
        os.getenv('ENVIRONMENT')
    )
    app.config.from_object(config_class_name)

    app.teardown_appcontext(cleanup)
    return app


def cleanup(_):
    Session.remove()


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', debug=True)
