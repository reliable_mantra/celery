import os
import logging
from tasks import queue


logger = logging.getLogger(__name__)


@queue.task()
def log_task(message):
    logger.info(message)
    return None
