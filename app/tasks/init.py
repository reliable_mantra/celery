import os
from .setup import CeleryQueue


def make_queue(app_name=__name__):
    celery = CeleryQueue(
        app_name,
        broker=os.getenv('CELERY_BROKER_URL')
    )
    return celery


queue = make_queue()


def init_queue(queue, app):
    queue.conf.update(app.config)
    TaskBase = queue.Task
    class ContextTask(TaskBase):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    queue.Task = ContextTask
