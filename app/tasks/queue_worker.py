import os
import sys


# Sorry for path manipulation it is the smartest thing we could think of
# It is required so that below import from parent dir could be possible
sys.path.insert(
    0,
    os.path.join(
        os.path.dirname(os.path.abspath(__file__)), '..'
    )
)

from app.app import create_app
from tasks import queue, init_queue
# from app.tasks import queue


app = create_app()
init_queue(queue, app)
