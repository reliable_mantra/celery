from celery import Celery


class CeleryQueue(Celery):

    def gen_task_name(self, name, module):
        module = f'tasks.{module}'
        return super(CeleryQueue, self).gen_task_name(name, module)
