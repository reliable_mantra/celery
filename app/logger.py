import os
import logging
import logging.config


level = os.getenv('LOGGING_LEVEL', 'DEBUG')
handlers = ['stream']

logger_config = {
    'disable_existing_loggers': False,
    'version': 1,
    'formatters': {
        'default': {
            'format': (
                '%(asctime)s | %(process)d %(levelname)-8s %(name)-15s : '
                '%(message)s'
            )
        }
    },
    'handlers': {
        'stream': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': 'ext://sys.stdout'
        }
    },
    'loggers': {
        '': {
            'level': level,
            'handlers': handlers
        }
    }
}


def setup_logger():
    logging.config.dictConfig(logger_config)
    logger = logging.getLogger(__name__)
    logger.info(f'Logging is set up with level `{level}`')
