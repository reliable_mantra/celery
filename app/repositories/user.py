from database.models import User
from database.session import Session


class UserRepository(object):
    def all(self):
        return Session.query(User).all()

    def get(self, user_id: int):
        return Optional[User], Session.query(User).get(user_id)

    def save(self, user: User):
        Session.add(user)
        Session.commit()
        return user

    def delete(self, user: User):
        Session.delete(user)
        Session.commit()
        return user

    def is_dirty(self, user: User):
        return Session.is_modified(user)
