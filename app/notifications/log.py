import logging
from blinker import signal


log = signal('log')
logger = logging.getLogger(__name__)

@log.connect
def log_handler(name):
    logger.info(name)
