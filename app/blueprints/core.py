from flask import Blueprint
from notifications import log, log_handler
from tasks.events import log_task


core = Blueprint('core', import_name=__name__)


@core.route('/process/<name>')
def process(name):
    log.send(name)
    log_handler(name)
    log_task.delay(name)
    return name