import os


class BaseConfig:
    """Base configuration"""
    TESTING = False


class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    DEBUG = True
    DATABASE_URI = os.getenv('DATABASE_URI', '')


class TestingConfig(BaseConfig):
    """Testing configuration"""
    TESTING = True


class ProductionConfig(BaseConfig):
    """Production configuration"""
    pass


def get_config_class_name(environment: str):
    config_class = {
        'development': 'config.DevelopmentConfig',
        'production': 'config.ProductionConfig',
        'test': 'config.TestConfig'
    }
    try:
        return config_class[environment.lower()]
    except KeyError:
        raise RuntimeError(
            'Invalid configuration! Set `ENVIRONMENT` environment variable.'
        )


def get_config():
    environment = os.getenv('ENVIRONMENT')
    if environment == 'development':
        return DevelopmentConfig
    elif environment == 'production':
        return ProductionConfig
    elif environment == 'test':
        return TestConfig
    else:
        raise RuntimeError(
            'Invalid configuration! Set `ENVIRONMENT` environment variable'
        )
