echo "Waiting for postgres..."

until PGPASSWORD=$POSTGRES_PASSWORD psql -h "database" -U "$POSTGRES_USER" -c '\q'; do
  sleep 1
done

echo "PostgreSQL started"

# Init database
cd /usr/src/root/app/database && python init.py $DATABASE_URI

python /usr/src/root/app/app.py
